<?php

namespace Tests\Smorken\ArrayCache\Stubs;

use Smorken\ArrayCache\Key;

class KeyStub extends Key implements \Smorken\ArrayCache\Contracts\Key
{
    public function get(array|string $key): string
    {
        return 'Stub.'.parent::get($key);
    }
}

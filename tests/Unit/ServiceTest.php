<?php

namespace Tests\Smorken\ArrayCache\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\ArrayCache\Contracts\Service;
use Smorken\ArrayCache\Repository;
use Tests\Smorken\ArrayCache\Stubs\KeyStub;

class ServiceTest extends TestCase
{
    public function testGetCanReturnPutValue(): void
    {
        $sut = $this->getSut();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertEquals('bar', $sut->get([$this, 'foo']));
    }

    public function testGetInstanceReturnsTheSameValue(): void
    {
        $sut = $this->getSut();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertEquals('bar', $sut->get([$this, 'foo']));
        $sut2 = $this->getSut();
        $this->assertSame($sut->getRepository(), $sut2->getRepository());
        $this->assertEquals('bar', $sut2->get([$this, 'foo']));
    }

    public function testGetReturnsDefaultWhenNotFound(): void
    {
        $sut = $this->getSut();
        $this->assertNull($sut->get('test'));
        $this->assertFalse($sut->get('test', false));
    }

    public function testHasIsFalseWhenNotSet(): void
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->has([$this, 'foo']));
    }

    public function testHasIsTrueWhenSet(): void
    {
        $sut = $this->getSut();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertTrue($sut->has([$this, 'foo']));
    }

    public function testHasIsTrueWhenSetAndNull(): void
    {
        $sut = $this->getSut();
        $sut->put([$this, 'foo'], null);
        $this->assertTrue($sut->has([$this, 'foo']));
    }

    public function testResetCanClearValue(): void
    {
        $sut = $this->getSut();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertEquals('bar', $sut->get([$this, 'foo']));
        $sut->reset();
        $this->assertNull($sut->get([$this, 'foo']));
    }

    public function testSetKeyCanUseDifferentKeyProvider(): void
    {
        $sut = $this->getSut();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertTrue($sut->has([$this, 'foo']));
        $sut->getRepository()->setKey(new KeyStub());
        $this->assertFalse($sut->has([$this, 'foo']));
    }

    protected function getSut(): Service
    {
        return new \Smorken\ArrayCache\Service(
            Repository::getInstance()
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->getSut()->reset();
    }
}

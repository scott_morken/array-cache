<?php

namespace Tests\Smorken\ArrayCache\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\ArrayCache\Repository;
use Tests\Smorken\ArrayCache\Stubs\KeyStub;

class RepositoryTest extends TestCase
{
    public function testGetCanReturnPutValue(): void
    {
        $sut = Repository::getInstance();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertEquals('bar', $sut->get([$this, 'foo']));
    }

    public function testGetInstanceReturnsTheSameValue(): void
    {
        $sut = Repository::getInstance();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertEquals('bar', $sut->get([$this, 'foo']));
        $sut2 = Repository::getInstance();
        $this->assertSame($sut, $sut2);
        $this->assertEquals('bar', $sut2->get([$this, 'foo']));
    }

    public function testGetReturnsDefaultWhenNotFound(): void
    {
        $sut = Repository::getInstance();
        $this->assertNull($sut->get('test'));
        $this->assertFalse($sut->get('test', false));
    }

    public function testHasIsFalseWhenNotSet(): void
    {
        $sut = Repository::getInstance();
        $this->assertFalse($sut->has([$this, 'foo']));
    }

    public function testHasIsTrueWhenSet(): void
    {
        $sut = Repository::getInstance();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertTrue($sut->has([$this, 'foo']));
    }

    public function testHasIsTrueWhenSetAndNull(): void
    {
        $sut = Repository::getInstance();
        $sut->put([$this, 'foo'], null);
        $this->assertTrue($sut->has([$this, 'foo']));
    }

    public function testResetCanClearValue(): void
    {
        $sut = Repository::getInstance();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertEquals('bar', $sut->get([$this, 'foo']));
        Repository::reset();
        $sut = Repository::getInstance();
        $this->assertNull($sut->get([$this, 'foo']));
    }

    public function testSetKeyCanUseDifferentKeyProvider(): void
    {
        $sut = Repository::getInstance();
        $sut->put([$this, 'foo'], 'bar');
        $this->assertTrue($sut->has([$this, 'foo']));
        $sut->setKey(new KeyStub());
        $this->assertFalse($sut->has([$this, 'foo']));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Repository::reset();
    }
}

<?php

namespace Tests\Smorken\ArrayCache\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\ArrayCache\Key;

class KeyTest extends TestCase
{
    public function testArrayWithArray(): void
    {
        $sut = new Key();
        $this->assertEquals('foo.bar.biz', $sut->get([['foo', 'bar'], 'biz']));
    }

    public function testArrayWithObject(): void
    {
        $sut = new Key();
        $this->assertEquals('Tests\Smorken\ArrayCache\Unit\KeyTest.foo', $sut->get([$this, 'foo']));
    }

    public function testSimpleArray(): void
    {
        $sut = new Key();
        $this->assertEquals('foo.bar', $sut->get(['foo', 'bar']));
    }

    public function testSimpleString(): void
    {
        $sut = new Key();
        $this->assertEquals('foo', $sut->get('foo'));
    }
}

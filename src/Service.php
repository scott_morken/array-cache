<?php

namespace Smorken\ArrayCache;

use Smorken\ArrayCache\Contracts\Repository;
use Smorken\Service\Services\BaseService;

/**
 * @method mixed get(array|string $key, mixed $default = null)
 * @method bool has(array|string $key)
 * @method void put(array|string $key, mixed $value)
 */
class Service extends BaseService implements \Smorken\ArrayCache\Contracts\Service
{
    public function __construct(protected Repository $repository, array $services = [])
    {
        parent::__construct($services);
    }

    public function __call(string $method, array $args): mixed
    {
        return call_user_func_array([$this->getRepository(), $method], $args);
    }

    public function getRepository(): Repository
    {
        return $this->repository;
    }

    public function reset(): void
    {
        $cls = $this->getRepository()::class;
        $cls::reset();
        $this->repository = $cls::getInstance();
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 8:57 AM
 */

namespace Smorken\ArrayCache;

use Smorken\ArrayCache\Contracts\Repository;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(Repository::class, function ($app) {
            return \Smorken\ArrayCache\Repository::getInstance();
        });
        $this->app->bind(\Smorken\ArrayCache\Contracts\Service::class, function ($app) {
            return new Service($app[Repository::class]);
        });
    }
}

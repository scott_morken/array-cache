<?php

namespace Smorken\ArrayCache;

class Key implements \Smorken\ArrayCache\Contracts\Key
{
    public function get(string|array $key): string
    {
        if (is_array($key)) {
            return implode('.', array_map(function ($v) {
                if (is_object($v)) {
                    return $v::class;
                }
                if (is_array($v)) {
                    return implode('.', $v);
                }

                return (string) $v;
            }, $key));
        }

        return $key;
    }
}

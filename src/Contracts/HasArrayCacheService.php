<?php

namespace Smorken\ArrayCache\Contracts;

interface HasArrayCacheService
{
    public function fromCache(string|array $key, mixed $default = null): mixed;

    public function getArrayCacheService(): Service;

    public function hasCachedKey(string|array $key): bool;

    public function toCache(string|array $key, mixed $value): void;
}

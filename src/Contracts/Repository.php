<?php

namespace Smorken\ArrayCache\Contracts;

interface Repository
{
    public function get(string|array $key, mixed $default = null): mixed;

    public function getKey(): Key;

    public function has(string|array $key): bool;

    public function put(string|array $key, mixed $value): void;

    public function setKey(Key $key): void;

    public static function getInstance(): Repository;

    public static function reset(): void;
}

<?php

namespace Smorken\ArrayCache\Contracts;

interface Key
{
    public function get(string|array $key): string;
}

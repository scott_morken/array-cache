<?php

namespace Smorken\ArrayCache\Contracts;

use Smorken\Service\Contracts\Services\BaseService;

/**
 * @method mixed get(array|string $key, mixed $default = null)
 * @method bool has(array|string $key)
 * @method void put(array|string $key, mixed $value)
 */
interface Service extends BaseService
{
    public function __call(string $method, array $args): mixed;

    public function getRepository(): Repository;

    public function reset(): void;
}

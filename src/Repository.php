<?php

namespace Smorken\ArrayCache;

use Smorken\ArrayCache\Contracts\Key;

class Repository implements \Smorken\ArrayCache\Contracts\Repository
{
    protected static ?\Smorken\ArrayCache\Contracts\Repository $instance = null;

    protected array $cache = [];

    protected ?Key $key = null;

    final protected function __construct() {}

    public static function getInstance(): \Smorken\ArrayCache\Contracts\Repository
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public static function reset(): void
    {
        self::$instance = null;
    }

    public function get(array|string $key, mixed $default = null): mixed
    {
        $key = $this->keyToString($key);

        return $this->cache[$key] ?? $default;
    }

    public function getKey(): Key
    {
        if ($this->key === null) {
            $this->key = new \Smorken\ArrayCache\Key();
        }

        return $this->key;
    }

    public function setKey(Key $key): void
    {
        $this->key = $key;
    }

    public function has(array|string $key): bool
    {
        $key = $this->keyToString($key);

        return array_key_exists($key, $this->cache);
    }

    public function put(array|string $key, mixed $value): void
    {
        $key = $this->keyToString($key);
        $this->cache[$key] = $value;
    }

    protected function keyToString(array|string $key): string
    {
        return $this->getKey()->get($key);
    }
}

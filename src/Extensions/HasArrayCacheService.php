<?php

namespace Smorken\ArrayCache\Extensions;

use Smorken\ArrayCache\Contracts\Service;

trait HasArrayCacheService
{
    public function fromCache(string|array $key, mixed $default = null): mixed
    {
        return $this->getArrayCacheService()->get([$this, $key], $default);
    }

    public function getArrayCacheService(): Service
    {
        return $this->getService(Service::class);
    }

    public function hasCachedKey(string|array $key): bool
    {
        return $this->getArrayCacheService()->has([$this, $key]);
    }

    public function toCache(string|array $key, mixed $value): void
    {
        $this->getArrayCacheService()->put([$this, $key], $value);
    }
}
